# IntranetChic

## Évolution du Projet (4 phases principales)

###    [Étape de lancement](#-étape-de-lancement)
Une étape de lancement du projet a été mise en place en amont de ces 4 phases. 
### 1. [Maquettage](#etape-1-:-maquettage)
### 2. [Conception](#étape-2-:-conception)
### 3. [Réalisation](#étape-3-:-réalisation)
### 4. [Présentation du prototype](#étape-4-:-présentation-de-la-maquette)

## Étape de lancement 
- État des lieux du site intranet
     - Prise de connaissance et analyse des différents menus et rubriques déjà mis en place sur le site

### Outils :
- xMind
     - Réalisation de cartes mentales pour avoir une vision d'ensemble de la structure générale de chaque onglet du site
- Excel
     - Mise en place de 5 colonnes pour faciliter le RDV avec le service communication :
          - A Conserver
          - A Valider
          - Qui est responsable de la donnée ?
          - Commentaires
          - Liens

## Etape 1 : Maquettage

### Proposition de menu :
- Accueil
     - Affichage du titre l'Intranet du CHIC au-dessus d'un barre de recherche
     - Affichage d'un bouton pour acceder aux Gardes et aux Astreintes
     - Affichage d'un bandeau "Flash Info" défilant
     - Affichage des événements récents
     - Affichage des actualités récentes
- Services en ligne
     - Affichage des différents services sur la page sous forme de cartes
     - Accès à la recherche d'un service en particulier avec une barre de recherche à droite de l'écran
- Documentation
     - Mise en place des différents sujets sur la page avec les titres des thèmes de documentation
     - Possibilité de cliquer sur un titre pour avoir accès à toute la documentation qui lui est associé
- Annuaire
     - Accès à l'annuaire de l'hôpital
     - Accès à l'aide sur l'utilisation du téléphone
- Le CHIC
- Ressources humaines

### Proposition de footer :
- Gauche
     - Site Internet du CHIC
     - Système d'information qualité
     - Charte d'utilisation des moyens informatiques
- Droite
     - Support informatique
     - TEL
     - MAIL
     - Contact Maître de toile
- Copyright CHIC

### Outils : 
- Balsamiq
     - Proposition de maquettes pour les différentes pages du site

## Étape 2 : Conception
.

## Étape 3 : Réalisation

## Étape 4 : Présentation du prototype





